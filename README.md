# Edify Mobile Application

**Note:** Current application requires internet connection for all actions. We still need to validate in case it hasn't one.
Eventually we'll need to keep local changes and poll them to compare with the server so that we can update the server
if updates are found local. Use a `only_local` flag to distinguish them.

## Environment
* Install Xcode `xcode-select --install`
* Install NVM `curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.0/install.sh | bash`
* Source bash `source ~/.bashrc`
* Install Node `nvm install stable`
* Use Node 4.2.3 `nvm use stable`
* Install Cordova `sudo npm install -g cordova`
* Install or update npm `npm install npm -g`
* Install [ios-deploy-tools](https://www.npmjs.com/package/ios-deploy) `npm install -g ios-deploy` or if on El Capitan use `sudo npm install --unsafe-perm=true -g ios-deploy`
* Install Ionic `sudo npm install -g ionic` or if on El Capitan add `--unsafe-perm=true`
* Install `sudo npm install -g ios-sim`
* Optional `cordova telemetry on`
* Add platform

```
ionic platform add ios
ionic platform add android
```

* Install with npm npm `install`
* Fix sass package

```
npm uninstall --save-dev gulp-sass
npm install --save-dev gulp-sass@2
```

* Build and emulate

```
sudo ionic cordova build ios
# or
sudo ionic cordova build android

# sudo ionic cordova emulate ios

# to run on device with livereload and logs use:
sudo ionic cordova run android -lcs
```

* Ref: http://ionicframework.com/docs/guide/installation.html

## OneSignal
* Follow instructions from [Notification Service Extension](https://documentation.onesignal.com/v3.0/docs/ios-sdk-setup#section-1-add-notification-service-extension-recommend-)
* Follow instructions from [SDK setup](https://documentation.onesignal.com/v3.0/docs/ionic-sdk-setup)
* Note that to add the OneSignal Framework `Build Phases` --> `Link Binary With Libraries` you'll need to press on `+` then press on `Add others`. Now under your project --> `platforms` --> `iOS` --> `Pods` you can now select `OneSignal` --> `iOS_SDK` --> `OneSignalSDK` --> `Framework` and now select `onesignal.framework`
* Also close the XCode porject and open a new one which you'll find in the same dir but with a different extension: `Edify.xcworkspace`
* You might need to run `sudo cp -r ~/Library/MobileDevice/ /Library/MobileDevice/` if you're getting error with the prov profiles

## Testing
* Test on browser (faster) `ionic server`
* Test on emulator `ionic emulate ios -l -s -c`
* Test on device (plugins compatible) `ionic run ios -l -s -c`
* Read about it [here](http://ionicframework.com/docs/cli/run.html)

## Dev Tools
* Ionic Framework
* Ionic Platform

## Notes
* Manage dependencies with https://www.npmjs.com/package/npm-check

## Specs

```
Your system information:

Cordova CLI: 6.4.0
Ionic Framework Version: 1.3.2
Ionic CLI Version: 2.1.4
Ionic App Lib Version: 2.1.2
ios-deploy version: 1.9.0
ios-sim version: 5.0.10
OS: Mac OS X Sierra
Node Version: v4.2.3
Xcode version: Xcode 8.2.1 Build version 8C1002
```

## Plugin List

* `cordova-custom-config 3.1.2 "cordova-custom-config"`
* `cordova-plugin-app-event 1.2.0 "Application Events"`
* `cordova-plugin-camera 2.3.1 "Camera"`
~~* `cordova-plugin-compat 1.1.0 "Compat"`~~
* `cordova-plugin-device 1.1.4 "Device"`
* `cordova-plugin-file-transfer 1.6.1 "File Transfer"`
* `cordova-plugin-inappbrowser 1.6.2-dev "InAppBrowser"`
* `cordova-plugin-splashscreen 4.0.1 "Splashscreen"`
* `cordova-sms-plugin 0.1.11 "Cordova SMS Plugin"`
* `de.appplant.cordova.plugin.local-notification 0.8.5-dev "LocalNotification"`
* `onesignal-cordova-plugin 2.1.2 "OneSignal Push Notifications"`
The local notification plugin was installed through:

```
cordova plugin add https://github.com/katzer/cordova-plugin-local-notifications#ios10
```

## Troubleshooting
* For: node-sass build fails node -v v4.2.3, use:

```
npm uninstall --save-dev gulp-sass
npm install --save-dev gulp-sass@2
```

* Error installing ios-deploy

```
stderr: xcode-select: error: tool 'xcodebuild' requires Xcode, but active developer directory '/Library/Developer/CommandLineTools' is a command line tools instance
```
Fix with https://github.com/nodejs/node-gyp/issues/569#issuecomment-255589932
