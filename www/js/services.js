var mod = angular.module('edify.services', [
    'edify.services.userServices',
    'edify.services.resources',
    'edify.services.LinkedAccountsServices',
    'edify.services.notification_poll',
    'edify.utils'
]);