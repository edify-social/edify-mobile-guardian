var mod = angular.module('edify.controllers.app_list', []);

mod.controller('AppListController', function(
    $scope,
    $ionicModal,
    $state,
    ApiEndpoint,
    $http,
    $stateParams
) {

    $scope.hasNotifs = true;

    $scope.appInit = function() {
        $scope.edify_id = $stateParams.index;

        $scope.app_list_req($scope.edify_id);
    };

    $scope.app_list_req = function (edify_id) {
		var url = ApiEndpoint.url + 'app_list?id=' + edify_id;
		return $http({
		  method: 'GET',
		  url: url
		})
		.success(meSuccessFn).error(meErrorFn);

		// Success and Error functions
		function meSuccessFn(data) {
			$scope.app_list = data.data.applications;
            window.localStorage.setItem("app_list", $scope.app_list)
		}
		function meErrorFn(data, error, status, headers, config) {
			console.error('Info requesting error: ' + error + ', ' + JSON.stringify(data));
		}
	}

    /****************************************************************************
     *
     * Remove Notification
     *
     ****************************************************************************/

    $scope.remove_notif = function(alert_id){
        console.log("Remove notif request sent for notif: " + alert_id);
        $scope.notification_list = window.localStorage.getItem("notifications_" + $scope.pin);
        var notifications = JSON.parse($scope.notification_list);
        var object_place = 0;
        var current = {};
        angular.forEach(notifications, function(v, k){
            if(alert_id == v.id){
                current = v;
                object_place = k;
            }
        });
        notifications[object_place]['status'] = 'deactivated';
        notifications[object_place]['feedback'] = 'read';
        notifications[object_place]['is_active'] = true; // Set true to keep receiving updates from this warning
        var headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        };
        var url = ApiEndpoint.url + "update_notifications";
        var postData =
            {
                "pin": $scope.pin,
                "inactive_warning": true,
                "id": alert_id
            };
        return $http({
            method: 'POST',
            url: url,
            headers: headers,
            data: postData
        }).success(removeSuccessFn).error(removeErrorFn);
        function removeSuccessFn(data) {
            notifications.splice(object_place, 1);
            window.localStorage.setItem("notifications_" + $scope.pin, JSON.stringify(notifications));
            myNotifUpdt = JSON.parse(window.localStorage.getItem("notifications_" + $scope.pin));
            $rootScope.$broadcast('my_notifications:update', myNotifUpdt);
            //$scope.alertInit();
            $ionicLoading.hide();
        }
        function removeErrorFn(data, error) {
            $ionicLoading.hide();
            console.error('Register error: ' + error + ', ' + JSON.stringify(data));
            //$state.go('app.device_messages', { 'index': $scope.sa_id });
        }
    };

    $scope.rm_notif = function(){
        if($scope.alert_id && $scope.alert_id > 0) {
            $scope.remove_notif($scope.alert_id);
        }
    };
});
