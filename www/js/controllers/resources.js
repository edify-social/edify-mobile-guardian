var mod = angular.module('edify.controllers.resources', []);

mod.controller('resourceController', function(
    $scope,
    $ionicModal,
    $state,
    $timeout,
    ResourceService
) {

    ResourceService.getData();
    $scope.resourceService = JSON.parse(window.localStorage.getItem("edify_resource_list"));

    $scope.openApplication = function (url) {
      window.open(url, '_blank', 'location=yes');
    };
});
