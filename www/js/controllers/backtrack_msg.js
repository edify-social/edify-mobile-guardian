var mod = angular.module('edify.controllers.backtrack_msg', []);

// Empty on purpose - waiting for removal - used by badges
mod.controller('BacktrackedMessageAlertController', function(
    $scope,
    $state,
    $stateParams,
    $ionicLoading,
    ApiEndpoint,
    $http,
    $rootScope,
    $ionicPopup
) {

    /****************************************************************************
     *
     * Initialize Alerts Request
     *
     ****************************************************************************/

    $scope.alertInit = function(){
        $scope.sa_id = $stateParams.index;
        $scope.pin = $stateParams.pin;
        $scope.my_notification_list = window.localStorage.getItem("notifications_backtrack_" + $scope.pin);
        var stored_notifications = '';
        var alert_id = $stateParams.alert_id;

        if($scope.my_notification_list === null){
            window.localStorage.setItem("notifications_backtrack_" + $scope.pin, []);
            stored_notifications = [];
        }
        else if($scope.my_notification_list.length == 0) {
            stored_notifications = [];
        } else {
            stored_notifications = JSON.parse($scope.my_notification_list);
        }
        $scope.alert = stored_notifications.filter(function ( obj )
        {
            if(obj.id == alert_id){
                return obj;
            } else {
                return false;
            }
        });

        if($scope.alert[0].feedback){
            $scope.is_saved = true;
        } else {
            $scope.is_saved = false;
        }


    };

    setTimeout(function() {
        var element = document.querySelector('#start_here');
        element.scrollIntoView();
    }, 49);

    /****************************************************************************
     *
     * Remove Notification
     *
     ****************************************************************************/

    $scope.remove_notif = function(index){
        var notifications = JSON.parse($scope.my_notification_list);
        var object_place = 0;
        var current = {};
        angular.forEach(notifications, function(v, k){
            if($scope.alert[0].id == v.id){
                current = v;
                object_place = k;
            }
        });
        notifications[object_place]['status'] = 'removed';
        notifications[object_place]['feedback'] = 'deleted';
        notifications[object_place]['is_active'] = false;
        // Update notification list on server
        var headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        };
        var url = ApiEndpoint.url + "update_notifications";
        var postData =
            {
                "pin": $scope.pin,
                "notifications": notifications
            };
        return $http({
            method: 'POST',
            url: url,
            headers: headers,
            data: postData
        }).success(removeSuccessFn).error(removeErrorFn);
        function removeSuccessFn(data) {
            notifications.splice(object_place, 1);
            window.localStorage.setItem("notifications_backtrack_" + $scope.pin, JSON.stringify(notifications));
            myNotifUpdt = JSON.parse(window.localStorage.getItem("notifications_backtrack_" + $scope.pin));
            $rootScope.$broadcast('my_notifications:update', myNotifUpdt);
            $ionicLoading.hide();
            $state.go('app.child_alert_list', { 'index': index });
        }
        function removeErrorFn(data, error) {
            $ionicLoading.hide();
            console.error('Register error: ' + error + ', ' + JSON.stringify(data));
            $state.go('app.child_alert_list', { 'index': index });
        }
    };

    $scope.remove_saved_notif = function(index){
        var notifications = JSON.parse($scope.my_notification_list);
        var object_place = 0;
        var current = {};
        angular.forEach(notifications, function(v, k){
            if($scope.alert[0].id == v.id){
                current = v;
                object_place = k;
            }
        });
        notifications[object_place]['status'] = 'removed';
        notifications[object_place]['feedback'] = 'saved';
        notifications[object_place]['is_active'] = false;
        // Update notification list on server
        var headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        };
        var url = ApiEndpoint.url + "update_notifications";
        var postData =
            {
                "pin": $scope.pin,
                "notifications": notifications
            };
        return $http({
            method: 'POST',
            url: url,
            headers: headers,
            data: postData
        }).success(removeSuccessFn).error(removeErrorFn);
        function removeSuccessFn(data) {
            notifications.splice(object_place, 1);
            window.localStorage.setItem("notifications_backtrack_" + $scope.pin, JSON.stringify(notifications));
            myNotifUpdt = JSON.parse(window.localStorage.getItem("notifications_backtrack_" + $scope.pin));
            $rootScope.$broadcast('my_notifications:update', myNotifUpdt);
            $ionicLoading.hide();
            $state.go('app.att_req', { 'index': index });
        }
        function removeErrorFn(data, error) {
            $ionicLoading.hide();
            console.error('Register error: ' + error + ', ' + JSON.stringify(data));
            $state.go('app.child_alert_list', { 'index': index });
        }
    };

    /****************************************************************************
     *
     * Send to Resources
     *
     ****************************************************************************/

    $scope.goToResources = function() {
        $state.go('app.resources');
    };

    /****************************************************************************
     *
     * Initialize Save Notification
     *
     ****************************************************************************/

    $scope.save_notif = function(index) {

        // Save and take to list
        var notifications = JSON.parse($scope.my_notification_list);
        var object_place = 0;
        var current = {};
        angular.forEach(notifications, function(v, k){
            if($scope.alert[0].id == v.id){
                current = v;
                object_place = k;
            }
        });
        notifications[object_place]['status'] = 'on_device';
        notifications[object_place]['feedback'] = 'saved';
        notifications[object_place]['is_active'] = false;
        // Update notification list on server
        var headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        };
        var url = ApiEndpoint.url + "update_notifications";
        var postData =
            {
                "pin": $scope.pin,
                "notifications": notifications
            };
        return $http({
            method: 'POST',
            url: url,
            headers: headers,
            data: postData
        }).success(saveSuccessFn).error(saveErrorFn);
        function saveSuccessFn(data) {
            window.localStorage.setItem("notifications_backtrack_" + $scope.pin, JSON.stringify(notifications));
            myNotifUpdt = JSON.parse(window.localStorage.getItem("notifications_backtrack_" + $scope.pin));
            myNotifUpdt = JSON.parse(window.localStorage.getItem("notifications_backtrack_" + $scope.pin));
            $rootScope.$broadcast('my_notifications:update', myNotifUpdt);
            $ionicLoading.hide();
            $state.go('app.att_req', { 'index': index });
        }
        function saveErrorFn(data, error) {
            $ionicLoading.hide();
            console.error('Register error: ' + error + ', ' + JSON.stringify(data));
        }

    };

    /****************************************************************************
     *
     * Initialize Notification Actions
     *
     ****************************************************************************/

    $scope.removeNotification = function(index){
        $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });
        // Remove and go back to list
        $scope.remove_notif(index);
    };
    $scope.removeSavedNotification = function(index){
        $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });
        // Remove and go back to list
        $scope.remove_saved_notif(index);
    };
    $scope.savedMsgs = function(index){
        $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });
        // Remove and go back to list
        $scope.save_notif(index);
    };

    $scope.alreadySaved = function() {
        $ionicPopup.show({
            title: "<h2>Saved</h2>",
            subTitle: "Already saved",
            template: 'This alert has already been saved. Visit your Saved Alerts list to view it.',
            buttons: [
                {
                    text: 'Got it',
                    type: 'button-positive'
                }
            ]
        });
    }

});
