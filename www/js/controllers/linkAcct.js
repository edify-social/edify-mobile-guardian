var mod = angular.module('edify.controllers.linkAccount', []);

mod.controller("linkController", function(
  $scope,
  $state,
  $http,
  $stateParams,
  $cordovaCamera,
  $cordovaInAppBrowser,
  $timeout,
  $ionicPlatform,
  UserService,
  $ionicPopup,
  $ionicLoading,
  LinkedAccountService,
  ApiEndpoint,
  $cordovaSms,
  $rootScope,
  $ionicHistory
) {

    $scope.connected = true;
    $scope.alert_id = 0;
    $scope.info_alert_id = 0;
    $scope.backtrack = window.localStorage.getItem('backtrack_purchased');

    // SMS
    $scope.sms = {
        phnumber: '',
        message: 'Download Edify from https://play.google.com/store/apps/details?id=social.edify.mobile'
    };

    /****************************************************************************
     *
     * Navigation
     *
     ****************************************************************************/

    $scope.startApp = function() {
        $state.go('main');
    };
    $scope.next = function() {
        $ionicSlideBoxDelegate.next();
    };
    $scope.previous = function() {
        $ionicSlideBoxDelegate.previous();
    };
    // Called each time the slide changes
    $scope.slideChanged = function(index) {
        $scope.slideIndex = index;
    };
    $scope.goToConfigProfile = function(index){
        $state.go('app.sub_acct_config', { 'index': index });
    };
    $scope.goToInstaConfig = function(){
        $state.go('app.instagram_config', { 'child_id': $scope.sa_id });
    };
    $scope.goToFollowers = function(index){
        $state.go('app.lk_followers', { 'index': index });
    };
    $scope.goToFollowing = function(index){
        $state.go('app.lk_following', { 'index': index });
    };
    $scope.goToResources = function(index) {
        $state.go('app.resources', { 'index': index });
    };
    $scope.goToDeviceMessages = function(index) {
        $state.go('app.device_messages', { 'index': index });
    };
    $scope.goToAppList = function(index) {
        $scope.rm_info_notif();
        $state.go('app.app_list', { 'index': index });
    };
    $scope.savedMsgs = function(index) {
        $state.go('app.att_req', { 'index': index });
    };
    $scope.social_prof = function(index){
        $state.go('app.child_alert_list', { 'index': index });
    };
    $scope.goToBacktrackedeMessages = function(index){
        $state.go('app.backtracked', { 'index': index });
    };

    $scope.appProfile = function() {
        $ionicHistory.nextViewOptions({
            disableBack: true
        });
        $state.go('app.onboarding', { 'index': $scope.sa_id  });
    };
    $scope.device_connection_check = function() {
        var headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        };
        var url = ApiEndpoint.url + "check_connection";
        var postData =
            {
                "pin": $scope.pin
            };
        return $http({
            method: 'POST',
            url: url,
            headers: headers,
            data: postData
        }).success(checkSuccessFn).error(checkErrorFn);
        function checkSuccessFn(data) {
            console.log("Connection check request sent");

            $scope.connected = data.data.connected;

            if(data.data.connected){
                console.log("connected true")
            } else {
                $scope.rm_notif();
            }
        }
        function checkErrorFn(data, error) {
            console.error('Check error: ' + error + ', ' + JSON.stringify(data));
        }
    };

    /****************************************************************************
     *
     * Linked Accounts
     *
     ****************************************************************************/

    $scope.backtrack_check = function() {
        var headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        };
        var url = ApiEndpoint.url + "backtrack_check";
        var postData =
            {
                "pin": $scope.pin
            };
        return $http({
            method: 'POST',
            url: url,
            headers: headers,
            data: postData
        }).success(checkSuccessFn).error(checkErrorFn);
        function checkSuccessFn(data) {
            console.log("Check request sent");
            console.log(data);
            window.localStorage.setItem('backtrack_purchased', data.data.backtrack);
        }
        function checkErrorFn(data, error) {
            $ionicLoading.hide();
            console.error('Check error: ' + error + ', ' + JSON.stringify(data));
        }
    };

    // Current instance of our account - Mainly as a template helper
    $scope.current = {};

    // Activate swipe on buttons
    $scope.listCanSwipe = true;

    // Sub account id from params
    $scope.sa_id = $stateParams.index;
    $scope.linkedAccounts = JSON.parse(window.localStorage.getItem("local_account_list"));

    $scope.linkInit = function() {
        $scope.sa_id = $stateParams.index;
        var rawLinkedAccounts = JSON.parse(window.localStorage.getItem("local_account_list"));
        // Get current account set to object and account position through params id
        angular.forEach(rawLinkedAccounts, function(v, k){
            if($scope.sa_id == v.id){
                $scope.current = v;
                $scope.pin = v.pin;
                $scope.instagram_user = v.instagram_user;
                $scope.object_place = k;
                $scope.backtrack_status = v.backtrack_status;
            }
        });

        $scope.sms.phnumber = $scope.current.child_phone;
        $scope.badges();
        $scope.back_badges();
        $scope.backtrack_check();
        $scope.device_connection_check();
        $scope.backtrack = window.localStorage.getItem('backtrack_purchased');
    };

    $scope.$on('linked_accts:update', function(event,data) {
        $scope.linkInit();
        var sub_with_image = document.getElementById('subWith');
        var sub_without_image = document.getElementById('subWithout');
        if(sub_with_image){
          sub_with_image.src = $scope.current.image;
          sub_with_image.classList.remove('ng-hide');
          sub_without_image.classList.add('ng-hide');
        }
    });

    $scope.remove_acct = function(aid) {
        LinkedAccountService.removeAcct(aid, 0);
    };

    $scope.updateChildAcct = function(aid, newName, newPhone) {
        $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });
        LinkedAccountService.updateChildAccount(aid, newName, newPhone);
        $scope.current.name = newName;
        $scope.current.child_phone = newPhone;
    };

    $scope.showEditPopup = function() {
        $scope.data = {};

        // An elaborate, custom popup
        $ionicPopup.show({
            template:
            '<label id="name_lbl" style="color: red; display: none;" for="sa_acct_name">Name Required</label>' +
            '<input type="text" id="sa_acct_name" style="padding: 1em;" ng-model="data.sa_name" placeholder=' + $scope.current.name + '>' +
            '<label id="phone_child_lbl" style="color: red; margin-top: 1em; margin-bottom: -1em; display: none;" for="sa_acct_phone">Phone Number Required</label>' +
            '<input type="text" id="sa_acct_phone" style="padding: 1em; margin-top: 1em;" ng-model="data.child_phone" placeholder=' + $scope.current.child_phone + '>',
            title: '<h2>Edit Account</h2>',
            subTitle: 'Change name & phone number',
            scope: $scope,
            buttons: [
                {
                    text: 'Cancel',
                    type: 'button-assertive'
                },
                {
                    text: '<b>Save</b>',
                    type: 'button-positive',
                    onTap: function (e) {
                        if (!$scope.data.sa_name || !$scope.data.child_phone) {
                            e.preventDefault();
                            inpt_phone                      = document.getElementById("sa_acct_phone");
                            inpt_phone_lbl                  = document.getElementById("phone_child_lbl");
                            nm_inpt                         = document.getElementById("sa_acct_name");
                            nm_inpt_lbl                     = document.getElementById("name_lbl");
                            nm_inpt.style.border            = "1px solid red";
                            nm_inpt.style.marginTop         = "0";
                            inpt_phone.style.border         = "1px solid red";
                            inpt_phone_lbl.style.display    = "block";
                            nm_inpt_lbl.style.display       = "block";
                        } else {
                            $scope.updateChildAcct($scope.current.edify_id, $scope.data.sa_name, $scope.data.child_phone);
                        }
                    }
                }
            ]
        });
    };

    // badge test
    $scope.badge            = 0;
    $scope.back_badge       = 0;
    $scope.new_apps_badge   = 0;

    $scope.badges = function() {
        tmp_array = $scope.linkedAccounts;
        //angular.forEach(tmp_array, function(val){
            //console.log("entered badges");
            //val.badge = $scope.a_badge_count(val.pin);
        //});
        $scope.a_badge_count($scope.current.pin);
        window.localStorage.setItem("local_account_list", JSON.stringify($scope.linkedAccounts));
        $scope.linkedAccounts = JSON.parse(window.localStorage.getItem("local_account_list"));
    };
    $scope.a_badge_count = function(pin) {
        ctr = 0;
        //console.log("entered a badge: " + pin);
        //console.log(window.localStorage.getItem("notifications_" + pin));
        if(window.localStorage.getItem("notifications_" + pin)) {
            notification_list = JSON.parse(window.localStorage.getItem("notifications_" + pin));
        } else {
            notification_list = [];
        }
        warning = 0;
        info = 0;
        angular.forEach(notification_list, function(v){
            if(v.feedback != 'saved' && v.label != 'Warning' && v.label != 'Info') {
                ctr++;
            } else if(v.label == 'Warning'){
                warning = warning + 1;
                $scope.alert_id = v.id;
            } else if(v.label == 'Info'){
                info = info + 1;
                $scope.info_alert_id = v.id;
            }
        });
        if(warning > 0){
            $scope.disconnected = true;
        } else {
            $scope.disconnected = false;
        }
        if(ctr > 0){
            $scope.badge = ctr;
        }
        $scope.new_apps_badge = info;
        return ctr;
    };

    $scope.back_badges = function() {
        tmp_array = $scope.linkedAccounts;
        $scope.a_back_badge_count($scope.current.pin);
        window.localStorage.setItem("local_account_list", JSON.stringify($scope.linkedAccounts));
        $scope.linkedAccounts = JSON.parse(window.localStorage.getItem("local_account_list"));
    };
    $scope.a_back_badge_count = function(pin) {
        ctr = 0;
        if(window.localStorage.getItem("notifications_backtrack_" + pin)) {
            notification_list = JSON.parse(window.localStorage.getItem("notifications_backtrack_" + pin));
        } else {
            notification_list = [];
        }
        angular.forEach(notification_list, function(v){
            if(v.feedback != 'saved') {
                ctr++;
            }
        });
        if(ctr > 0){
            $scope.back_badge = ctr;
        }
        return ctr;
    };

    /****************************************************************************
     *
     * Remove Notification
     *
     ****************************************************************************/

    $scope.remove_notif = function(alert_id){
        console.log("Remove notif request sent for notif: " + alert_id);
        $scope.notification_list = window.localStorage.getItem("notifications_" + $scope.pin);
        var notifications = JSON.parse($scope.notification_list);
        var object_place = 0;
        var current = {};
        angular.forEach(notifications, function(v, k){
            if(alert_id == v.id){
                current = v;
                object_place = k;
            }
        });
        notifications[object_place]['status'] = 'deactivated';
        notifications[object_place]['feedback'] = 'read';
        notifications[object_place]['is_active'] = true; // Set true to keep receiving updates from this warning
        var headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        };
        var url = ApiEndpoint.url + "update_notifications";
        var postData =
            {
                "pin": $scope.pin,
                "inactive_warning": true,
                "id": alert_id
            };
        return $http({
            method: 'POST',
            url: url,
            headers: headers,
            data: postData
        }).success(removeSuccessFn).error(removeErrorFn);
        function removeSuccessFn(data) {
            notifications.splice(object_place, 1);
            window.localStorage.setItem("notifications_" + $scope.pin, JSON.stringify(notifications));
            myNotifUpdt = JSON.parse(window.localStorage.getItem("notifications_" + $scope.pin));
            $rootScope.$broadcast('my_notifications:update', myNotifUpdt);
            //$scope.alertInit();
            $ionicLoading.hide();
        }
        function removeErrorFn(data, error) {
            $ionicLoading.hide();
            console.error('Register error: ' + error + ', ' + JSON.stringify(data));
            //$state.go('app.device_messages', { 'index': $scope.sa_id });
        }
    };

    $scope.rm_notif = function(){
        if($scope.alert_id && $scope.alert_id > 0) {
            $scope.remove_notif($scope.alert_id);
        }
    };

    $scope.rm_info_notif = function(){
        if($scope.info_alert_id && $scope.info_alert_id > 0) {
            $scope.remove_notif($scope.info_alert_id);
        }
    };

    /****************************************************************************
    *
    * Platform Ready: Instragram and photo update.
    *
    ****************************************************************************/

    $ionicPlatform.ready(function() {

        $scope.updateAcctAvatar = function(newAvatar) {
            LinkedAccountService.updateAccountAvatar($scope.current.edify_id, newAvatar);
            $scope.myPopup.close();
            $scope.goToConfigProfile($scope.sa_id);
        };

        $scope.updatePicture = function() {

            var options = {
                quality : 75,
                destinationType : Camera.DestinationType.DATA_URL,
                sourceType : Camera.PictureSourceType.PHOTOLIBRARY,
                allowEdit : true,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 150,
                targetHeight: 150,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false
            };

            $cordovaCamera.getPicture(options).then(function(imageData) {
                $scope.imgURI = "data:image/jpeg;base64," + imageData;
                var with_image = document.getElementById('withImage');
                var without_image = document.getElementById('withoutImage');
                with_image.src = "data:image/jpeg;base64," + imageData;
                with_image.classList.remove('ng-hide');
                without_image.classList.add('ng-hide');
                $timeout( function(){ $scope.updateAcctAvatar($scope.imgURI); }, 500);
            }, function(err) {
                console.log(err)
            });
        };

        $scope.showAvatarPopup = function() {
          window.crnt = $scope.current;
            $scope.data = {};
            $scope.myPopup = $ionicPopup.show({
                template:
                    '<a href="#" ng-click="updatePicture()">' +
                    '<i class="addAvatar ion-plus-circled"></i>' +
                    '<img class="childImage myImage" id="withImage" ng-hide="current.image == null" ng-src="{{current.image}}" style="width: 150px; height: 150px; object-fit: cover;">' +
                    '<img class="childImage" id="withoutImage" ng-show="current.image == null" ng-src="img/face_default.png" style="width: 150px;">' +
                    '</a><br/>',
                title: '<h2>Change Avatar</h2>',
                subTitle: 'Add or update account avatar',
                scope: $scope,
                buttons: [
                  {
                      text: 'Cancel',
                      type: 'button-assertive',
                      onTap: function() {
                        $scope.myPopup.close();
                      }
                  }
                ]
            });
        };

        /***
         * TEST SMS
         */

        var options = {
            replaceLineBreaks: false,
            android: {
                intent: 'INTENT' // send SMS with the native android SMS messaging
            },
            ios: ''
        };
        $scope.sendSMS = function () {
            console.log("Open SMS app");
            $cordovaSms.send($scope.sms.phnumber, $scope.sms.message, options).then(function () {
                console.log('Success');
            }, function (error) {
                console.log('Error');
            });
        };

    }); // platform ready ends here

});
