var mod = angular.module('edify.controllers.home', []);

mod.controller("homeController", function(
  $scope,
  $http,
  $state,
  $cordovaCamera,
  $ionicPopup,
  $timeout,
  $localStorage,
  $ionicPlatform,
  LinkedAccountService,
  UserService,
  ApiEndpoint,
  $window,
  $ionicSlideBoxDelegate,
  $stateParams,
  $ionicHistory,
  $rootScope,
  $interval,
  ResourceService,
  NotificationPollService
) {

    // Poll for resources
    ResourceService.getData();

    // Temporarily Disabled until Push notifications are ready - good health check tho
    // - Removed device ready from notification service line 25
    // - checkData funtion has been disabled line 154
    NotificationPollService.checkData();

    /****************************************************************************
     *
     * Global Variables
     * Set from local storage
     *
     ****************************************************************************/

    $scope.uid              = window.localStorage.getItem("edify_user_id");
    $scope.tkn              = window.localStorage.getItem("edify_user_token");
    $scope.nme              = window.localStorage.getItem("edify_user_name");
    $scope.phn              = window.localStorage.getItem("edify_user_phone");
    $scope.eml              = window.localStorage.getItem("edify_user_email");
    $scope.player_id        = JSON.parse(window.localStorage.getItem("player_id"));
    $scope.push_token       = JSON.parse(window.localStorage.getItem("push_token"));
    $scope.badge            = 0;


    /****************************************************************************
     *
     * Linked Accounts
     * Watch for linked acct updates or set initial values
     *
     ****************************************************************************/

    var pre_linkedAccounts = window.localStorage.getItem("local_account_list");

    $scope.$on('linked_accts:update', function(event,data) {
        pre_linkedAccounts = window.localStorage.getItem("local_account_list");
        linkedAccounts = JSON.parse(pre_linkedAccounts);
        $scope.setHasAccounts();
        //NotificationPollService.checkData();
    });

    if(pre_linkedAccounts){
        $scope.linkedAccounts = JSON.parse(pre_linkedAccounts);
    } else {
        $scope.linkedAccounts = [];
    }


    /****************************************************************************
     *
     * Notifications
     * Watch for notification updates or set initial values
     *
     ****************************************************************************/

    $scope.$on('my_notifications:update', function(event,data) {
        $scope.badges();
        // Trigger local notification
    });

    /****************************************************************************
     *
     * Functions
     *
     ****************************************************************************/

    $scope.homeInit = function () {
        $interval(function(){
            NotificationPollService.checkData();
        },  10000);
    };

    $scope.badges = function() {
        tmp_array = $scope.linkedAccounts;
        angular.forEach(tmp_array, function(val){
            //console.log("entered badges");
            val.badge = $scope.a_badge_count(val.pin);
        });
        window.localStorage.setItem("local_account_list", JSON.stringify($scope.linkedAccounts));
        $scope.linkedAccounts = JSON.parse(window.localStorage.getItem("local_account_list"));
    };
    $scope.a_back_badge_count = function(pin) {
        ctr = 0;
        if(window.localStorage.getItem("notifications_backtrack_" + pin)) {
            notification_back_list = JSON.parse(window.localStorage.getItem("notifications_backtrack_" + pin));
        } else {
            notification_back_list = [];
        }
        angular.forEach(notification_back_list, function(v){
            if(v.feedback != 'saved') {
                ctr++;
            }
        });
        window.bklst = notification_back_list;
        return ctr;
    };
    $scope.a_badge_count = function(pin) {
        var ctr = 0;
        if(window.localStorage.getItem("notifications_" + pin)) {
            notification_list = JSON.parse(window.localStorage.getItem("notifications_" + pin));
        } else {
            notification_list = [];
        }
        angular.forEach(notification_list, function(v){
            if(v.feedback != 'saved') {
                ctr++;
            }
        });
        ctr_back = $scope.a_back_badge_count(pin);
        ctr = ctr + ctr_back;
        //window.pst_ctr = ctr;
        if(ctr > 0){
            $scope.badge = ctr;
        }
        return ctr;
    };

   /*
    * Has accounts setter
    */
    $scope.setHasAccounts = function () {
        $scope.updateRemoteAccounts();
        if($scope.linkedAccounts){
            if($scope.linkedAccounts.length > 0){
                $scope.hasLinkedAccts = false;
                $scope.$broadcast('scroll.refreshComplete');
            } else {
                $scope.hasLinkedAccts = true;
                $scope.$broadcast('scroll.refreshComplete');
            }
        } else {
            // Empty List
            $scope.$broadcast('scroll.refreshComplete');
        }
    };

    /*
     * Pull accounts from server
     */
    $scope.updateRemoteAccounts = function () {
        UserService.loadChildAccounts($scope.uid);

        pre_linkedAccounts = window.localStorage.getItem("local_account_list");
        if(pre_linkedAccounts){
            $scope.linkedAccounts = JSON.parse(pre_linkedAccounts);
        } else {
            $scope.linkedAccounts = [];
        }

        $scope.badges();
    };

   /*
    * Terms of Service Modal
    */
    $scope.tos = function(){
        $ionicPopup.show({
            title: "<h2>Terms of Service</h2>",
            subTitle: "Read and Accept",
            template: 'Types of Edify Accounts: Currently Edify offers free ' +
                      'accounts ("Free Account"). By using this mobile application ' +
                      '(the "application") or by using the Service, you agree to be ' +
                      'legally bound by the following Terms of Service as well as ' +
                      'the Edify online privacy policy /disclaimer  found at www..',
            buttons: [
                {
                    text: "Cancel",
                    type: 'button-assertive'
                },
                {
                    text: 'I Accept',
                    type: 'button-positive'
                }
            ]
        });
    };

   /*
    * Go to linked account profile screen
    */
   $scope.goToProfile = function(index){
       $ionicHistory.nextViewOptions({
           disableBack: true
       });
       $state.go('app.sub_acct_config', { 'index': index });
   };

   $scope.appProfile = function() {
       $ionicHistory.nextViewOptions({
           disableBack: true
       });
       $scope.ix = $stateParams.index;
       $state.go('app.onboarding', { 'index': next_id });
       //$state.go('app.sub_acct_config', { 'index': $scope.ix });
   };

  /****************************************************************************
   *
   * On Entering our Home Screen and Main Flow
   *
   ****************************************************************************/
  $scope.$on('$ionicView.enter', function(e) {
    $scope.setHasAccounts();
  });

  /*
   * Refresh Home
   */
  $scope.doHomeRefresh = function() {
    $scope.setHasAccounts();
    $scope.updateRemoteAccounts();
  };

  $scope.regPush = function(){
    var url = ApiEndpoint.url + 'register_push/';

    var reg = {
      id: $scope.uid,
      player_id: $scope.player_id,
      push_token: $scope.push_token
    };

    console.log("Registering device with player id: " + $scope.player_id);

    return $http({
      method: 'POST',
      url: url,
      data: reg
    })
    .success(regSuccessFn).error(regErrorFn);

    function regSuccessFn(data) {
      console.log(JSON.stringify(data));
    }

    function regErrorFn(data, error, status, headers, config) {
      console.error('Push register error: ' + error + ', ' + JSON.stringify(data));
    }
  };

  $scope.regPush();
  $scope.badges();
/****************************************************************************
 *
 * Background Mode
 *
 ****************************************************************************/

  // Run when the device is ready
  document.addEventListener('deviceready', function () {
      // Android customization
      // To indicate that the app is executing tasks in background and being paused would disrupt the user.
      // The plug-in has to create a notification while in background - like a download progress bar.
      cordova.plugins.backgroundMode.setDefaults({
          title:  'Edify Notifcations',
          text:   'Managing background notifications'
      });

      // Enable background mode
      cordova.plugins.backgroundMode.enable();

      // Battery improvements here
      // Called when background mode has been activated
      /*cordova.plugins.backgroundMode.onactivate = function () {

          // Set an interval of 3 seconds (3000 milliseconds)
          setInterval(function () {

              console.log('backgrounf notif')
              // If required

          }, 3000);
      }*/

      //$scope.regPush();

  }, false);

  /*$ionicPlatform.ready(function() {
    $scope.regPush();
  });*/


  /****************************************************************************
   *
   * Sub account creation
   *
   ****************************************************************************/
  $scope.showPopup = function() {
      $scope.data = {};
      $ionicPopup.show({
          template:
          '<label id="name_lbl" style="color: red; display: none;" for="sa_acct_name">Name Required</label>' +
          '<input type="text" id="sa_acct_name" style="padding: 1em;" ng-model="data.sa_name" placeholder="Name">' +
          '<label id="phone_child_lbl" style="color: red; margin-top: 1em; margin-bottom: -1em; display: none;" for="sa_acct_phone">Phone Number Required</label>' +
          '<input type="text" id="sa_acct_phone" style="padding: 1em; margin-top: 1em;" ng-model="data.child_phone" placeholder="Phone Number">',
          title: '<h2>Add Child Account</h2>',
          subTitle: 'All fields required.',
          scope: $scope,
          buttons: [
              {
                  text: 'Cancel',
                  type: 'button-assertive'
              },
              {
                  text: '<b>Save</b>',
                  type: 'button-positive',
                  onTap: function(e) {
                      if (!$scope.data.sa_name || !$scope.data.child_phone) {
                          e.preventDefault();
                          inpt_phone                      = document.getElementById("sa_acct_phone");
                          inpt_phone_lbl                  = document.getElementById("phone_child_lbl");
                          nm_inpt                         = document.getElementById("sa_acct_name");
                          nm_inpt_lbl                     = document.getElementById("name_lbl");
                          nm_inpt.style.border            = "1px solid red";
                          nm_inpt.style.marginTop         = "0";
                          inpt_phone.style.border         = "1px solid red";
                          inpt_phone_lbl.style.display    = "block";
                          nm_inpt_lbl.style.display       = "block";
                      } else {
                          $scope.createSubAcct($scope.data.sa_name, $scope.data.child_phone);

                      }
                  }
              }]
      });
  }; // showPopup

 /*
  * Create Sub Account
  */
  $scope.createSubAcct = function(sname, sphone_child){
      var usr_uid = window.localStorage.getItem("edify_user_id");
      LinkedAccountService.createAccount(usr_uid, sname, sphone_child, '', '', 0, 0, 0);
  };

});
