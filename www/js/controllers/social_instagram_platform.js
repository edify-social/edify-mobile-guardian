var mod = angular.module('edify.controllers.socialPlatform', []);

mod.controller("SocProfileController", function(
  $scope,
  $state,  
  $localStorage,
  $location,
  $stateParams,
  $ionicPopup,
  $ionicLoading,
  LinkedAccountService
) {

   /****************************************************************************
    *
    * Main Workflow Functions
    *
    ****************************************************************************/

   $scope.updateInstaCreds = function() {
       $ionicLoading.show({
           content: 'Loading',
           animation: 'fade-in',
           showBackdrop: true,
           maxWidth: 200,
           showDelay: 0
       });

       usrn = $scope.current.instaUsrn;
       pswd = $scope.current.instaPswd;

       LinkedAccountService.updateAccountInstagram($scope.edify_id, usrn, pswd);
   };

    // Grab token
    $scope.socInstaInit = function() {
        $scope.sa_id = $stateParams.child_id;
        $scope.current = {};
        $scope.linkedAccounts = JSON.parse(window.localStorage.getItem("local_account_list"));

        angular.forEach($scope.linkedAccounts, function(v, k){
            if($scope.sa_id == v.id){
              $scope.current = v;
              $scope.current.insta_user = v.instagram_user;
              $scope.object_place = k;
              $scope.edify_id = v.edify_id;
            }
        });

        if($scope.current.insta_user){
            $scope.placeholder = $scope.current.insta_user;
        } else {
            $scope.placeholder = 'Instagram email';
        }

    };

});
