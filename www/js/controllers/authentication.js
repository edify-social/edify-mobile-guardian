var mod = angular.module('edify.controllers.authentication', []);

mod.controller('authenticationController', function(
    $scope,
    $http,
    $state,
    ApiEndpoint,
    $ionicPopup,
    $ionicLoading,
    UserService
) {
    /****************************************************************************
     *
     * Helpers
     *
     ****************************************************************************/
    //Check if logged in. If user then send to home screen.
    $scope.checkLoggedIn = function(){
        if(window.localStorage.getItem("edify_user_token") != null && window.localStorage.getItem("user_token") !=
            'null' && window.localStorage.getItem("edify_user_token").length > 0){
            $state.go('app.overview');
        }
        else {
            if ($state.current.name != 'register'){
                $state.go('login');
            }
        }
    };

    /****************************************************************************
     *
     * Form Parameters
     *
     ****************************************************************************/

    // Form data for the register modal
    $scope.registerData = {
        "name": "",
        "email": "",
        "phone": "",
        "password": ""
    };

    //window.reg = $scope.registerData;
    $scope.password_confirmation = {
        "password": ""
    };

    // Form data for the update modal
    $scope.updateData = {
        "id": "",
        "email": "",
        "name": "",
        "phone": ""
    };

    // Form data for the login modal
    $scope.loginData = {
        "user": {
            "email": "",
            "password": ""
        }
    };

    /****************************************************************************
     *
     * Updating
     *
     ****************************************************************************/

    $scope.profile = function () {
        $scope.my_email = window.localStorage.getItem("edify_user_email");
        $scope.my_phone = window.localStorage.getItem("edify_user_phone");
        $scope.my_name = window.localStorage.getItem("edify_user_name");
        if($scope.my_phone == 'null' || $scope.my_phone == null || $scope.my_phone == 'undefined') {
            $scope.display_phone = 'No number added yet';
            $scope.my_phone = 'No number added yet';
        } else {
            $scope.display_phone = $scope.my_phone;
        }
    };

    // UPDATE user profile
    $scope.update = function() {
        $scope.uid = window.localStorage.getItem("edify_user_id");
        if($scope.updateData.name || $scope.updateData.email || $scope.updateData.phone){
            $scope.updateData.id = $scope.uid;
            UserService.updateUser($scope.updateData).then(
                updSuccess,
                updError
            );
        } else {
            $ionicPopup.alert({
                title: "<h2>Error</h2>",
                subTitle: 'Your profile was not updated',
                template: "No changes to submit."
            });
        }
    };

    function updSuccess(succ){
        $scope.my_email = window.localStorage.getItem("edify_user_email");
        $scope.my_phone = window.localStorage.getItem("edify_user_phone");
        $scope.my_name = window.localStorage.getItem("edify_user_name");
        console.log(succ);
    }

    function updError(err){
        console.log(err);
    }

    /****************************************************************************
     *
     * Registration
     *
     ****************************************************************************/

    // CREATE new user profile
    $scope.register = function() {
        // Setup the loader
        $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });
        UserService.createUser($scope.registerData).then(
            regSuccess,
            regError
        );
    };

    function regSuccess(succ){
        $ionicLoading.hide();
        $state.go('app.overview');
    }

    function regError(err){
        $ionicLoading.hide();
    }

    /****************************************************************************
     *
     * Login
     *
     ****************************************************************************/

    // Login
    $scope.login = function(){
        $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });
        UserService.loginUser($scope.loginData).then(
            loginSuccess,
            loginError
        );
    };// login

    function loginSuccess(succ){
        edify_id = window.localStorage.getItem("edify_user_id");
        UserService.loadUser(edify_id);
        $state.go('app.overview');
    }

    function loginError(err){
        $ionicLoading.hide();
        console.log(err);
    }

    function bypass(){
       $state.go('app.overview');
    }

    /****************************************************************************
     *
     * Logout
     *
     ****************************************************************************/

    // Logout
    $scope.logout = function() {
        $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });
        UserService.logoutUser().then(
            logoutSuccess,
            logoutError
        );
    };// Logout

    function logoutSuccess(succ){
        console.log(succ);
    }

    function logoutError(err){
        console.log(err);
    }


});
