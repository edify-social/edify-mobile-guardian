var mod = angular.module('edify.controllers.device_messages', []);
mod.controller('DeviceMessagesController', function(
    $scope,
    $state,
    $http,
    $stateParams,
    $ionicPopup,
    $ionicLoading,
    ApiEndpoint,
    LinkedAccountService,
    $rootScope,
    $ionicPlatform
) {

    $scope.product_owned = false;
    $scope.product_can_purchase = false;

    /****************************************************************************
     *
     * Initialize Alerts Request
     *
     ****************************************************************************/

    $scope.alertInit = function() {
        $scope.shouldShowDelete = false;
        $scope.shouldShowReorder = false;
        $scope.listCanSwipe = true;
        $scope.linkedAccounts = JSON.parse(window.localStorage.getItem("local_account_list"));
        $scope.sa_id = $stateParams.index;
        $scope.pin = '';
        $scope.alerts = [];
        var stored_notifications = '';
        angular.forEach($scope.linkedAccounts, function (v) {
            if ($scope.sa_id == v.edify_id) {
                $scope.pin = v.pin;
                $scope.backtrack_status = v.backtrack_status;
            }
        });
        $scope.backtrack = window.localStorage.getItem("backtrack_purchased");
        $scope.notification_list = window.localStorage.getItem("notifications_" + $scope.pin);
        if ($scope.notification_list === null) {
            $scope.notification_list = [];
            stored_notifications = [];
        } else if ($scope.notification_list.length == 0) {
            $scope.notification_list = [];
            stored_notifications = [];
        } else {
            stored_notifications = JSON.parse($scope.notification_list);
        }

        angular.forEach(stored_notifications, function (v) {
            if (v.feedback != 'saved' && v.label != 'Warning' && v.label != 'Info') {
                if($scope.alerts){
                    $scope.alerts.push(v);
                } else {
                    $scope.alerts[0] = v;
                }
            }
        });
        if ($scope.alerts) {
            if ($scope.alerts.length <= 0)
                $scope.hasNotifs = false;
            else
                $scope.hasNotifs = true;
        }

    };

    /****************************************************************************
     *
     * In App Purchases
     *
     ****************************************************************************/

    $ionicPlatform.ready(function() {

        $scope.isIOS = ionic.Platform.isIOS();
        $scope.isAndroid = ionic.Platform.isAndroid();
        $scope.currentPlatform = ionic.Platform.platform();

        $scope.initializeStore = function() {
            store.verbosity = store.INFO;

            // Android
            if($scope.isAndroid){
              store.register({
                  id:    "backtrack_device_01",
                  alias: "Backtrack",
                  type:  store.CONSUMABLE
              });

              store.ready(function() {
                  console.log("Store ready");
              });

              store.when("Backtrack").updated(function (product) {
                  product.owned ? $scope.product_owned = true : $scope.product_owned = false;
                  $scope.store_price = product.price;
                  $scope.store_alias = product.alias;
                  $scope.product_id = product.id;
                  console.log($scope.store_price); // null
                  console.log($scope.store_alias); // backtrack
                  console.log($scope.store_product_id); // undefined
                  console.log(product); // object
                  $scope.product_can_purchase = product.valid;
                  console.log(product.valid);
              });
            }

            // iOS
            if($scope.isIOS){
              store.register({
                  id:    "backtrack_consumable",
                  alias: "backtrack",
                  type:  store.CONSUMABLE
              });

              store.ready(function() {
                  console.log("Store ready");
              });

              store.when("backtrack").updated(function (product) {
                  product.owned ? $scope.product_owned = true : $scope.product_owned = false;
                  $scope.store_price = product.price;
                  $scope.store_alias = product.alias;
                  $scope.product_id = product.id;
                  console.log($scope.store_price);
                  console.log($scope.store_alias);
                  console.log($scope.store_product_id);
                  console.log(product);
                  $scope.product_can_purchase = product.valid;
              });
            }

            store.refresh();
        };

        $scope.request_backtrack = function(){
            LinkedAccountService.resetBacktrack($scope.sa_id, $scope.pin);
        };

        $scope.postPurchase = function() {
            var headers = {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            };
            var url = ApiEndpoint.url + "purchase_backtrack";
            var postData =
                {
                    "pin": $scope.pin
                };
            return $http({
                method: 'POST',
                url: url,
                headers: headers,
                data: postData
            }).success(removeSuccessFn).error(removeErrorFn);
            function removeSuccessFn(data) {
                console.log("Purchase shared with server");
                console.log(data);
            }
            function removeErrorFn(data, error) {
                $ionicLoading.hide();
                console.error('Register error: ' + error + ', ' + JSON.stringify(data));
            }
        };

        $scope.purchaseBacktrack = function() {
            store.order($scope.product_id);

            // Android
            if($scope.isAndroid){
              store.when("Backtrack").approved(function (order) {
                console.log('You just unlocked the Backtrack feature!');
                $scope.postPurchase();
                  $scope.product_owned = true;
                order.finish();
              });

              store.when("Backtrack").updated(function (product) {
                  product.owned ? $scope.product_owned = true : $scope.product_owned = false;
                  console.log("Store Updated");
                  $scope.store_price = product.price;
                  console.log(product);
              });

              store.ready(function() {
                console.log("Store ready");
              });
            }

            // iOS
            if($scope.isIOS){
              store.when("backtrack").approved(function (order) {
                console.log('You just unlocked the Backtrack feature!');
                $scope.postPurchase();
                  $scope.product_owned = true;
                order.finish();
              });

              store.when("backtrack").updated(function (product) {
                  product.owned ? $scope.product_owned = true : $scope.product_owned = false;
                  console.log("Store Updated");
                  $scope.store_price = product.price;
                  console.log(product);
              });

              store.ready(function() {
                console.log("Store ready");
              });
            }
        };

        $scope.initializeStore();

    }); // IonicPlatform.ready ends

    //$scope.initializeStore();

    /****************************************************************************
     *
     * Remove Notification
     *
     ****************************************************************************/

    $scope.remove_notif = function(alert_id){
        var notifications = JSON.parse($scope.notification_list);
        var object_place = 0;
        var current = {};
        angular.forEach(notifications, function(v, k){
            if(alert_id == v.id){
                current = v;
                object_place = k;
            }
        });
        notifications[object_place]['status'] = 'removed';
        notifications[object_place]['feedback'] = 'deleted';
        notifications[object_place]['is_active'] = false;
        var headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        };
        var url = ApiEndpoint.url + "update_notifications";
        var postData =
            {
                "pin": $scope.pin,
                "notifications": notifications
            };
        return $http({
            method: 'POST',
            url: url,
            headers: headers,
            data: postData
        }).success(removeSuccessFn).error(removeErrorFn);
        function removeSuccessFn(data) {
            notifications.splice(object_place, 1);
            window.localStorage.setItem("notifications_" + $scope.pin, JSON.stringify(notifications));
            myNotifUpdt = JSON.parse(window.localStorage.getItem("notifications_" + $scope.pin));
            $rootScope.$broadcast('my_notifications:update', myNotifUpdt);
            $scope.alertInit();
            $ionicLoading.hide();
        }
        function removeErrorFn(data, error) {
            $ionicLoading.hide();
            console.error('Register error: ' + error + ', ' + JSON.stringify(data));
            $state.go('app.device_messages', { 'index': $scope.sa_id });
        }
    };

    /****************************************************************************
     *
     * Initialize Save Notification
     *
     ****************************************************************************/

    $scope.save_notif = function(alert_id) {

        // Save and take to list
        var notifications = JSON.parse($scope.notification_list);
        var object_place = 0;
        var current = {};
        angular.forEach(notifications, function(v, k){
            if(alert_id == v.id){
                current = v;
                object_place = k;
            }
        });
        notifications[object_place]['status'] = 'on_device';
        notifications[object_place]['feedback'] = 'saved';
        notifications[object_place]['is_active'] = false;
        // Update notification list on server
        var headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        };
        var url = ApiEndpoint.url + "update_notifications";
        var postData =
            {
                "pin": $scope.pin,
                "notifications": notifications
            };
        return $http({
            method: 'POST',
            url: url,
            headers: headers,
            data: postData
        }).success(saveSuccessFn).error(saveErrorFn);
        function saveSuccessFn(data) {
            window.localStorage.setItem("notifications_" + $scope.pin, JSON.stringify(notifications));
            myNotifUpdt = JSON.parse(window.localStorage.getItem("notifications_" + $scope.pin));
            $rootScope.$broadcast('my_notifications:update', myNotifUpdt);
            $scope.alertInit();
            $ionicLoading.hide();
        }
        function saveErrorFn(data, error) {
            $ionicLoading.hide();
            console.error('Register error: ' + error + ', ' + JSON.stringify(data));
        }

    };

    /****************************************************************************
     *
     * Initialize Notification Actions
     *
     ****************************************************************************/

    $scope.removeNotification = function(alert_id){
        $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });
        $scope.remove_notif(alert_id);
    };

    $scope.savedMsgs = function(alert_id){
        $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });
        $scope.save_notif(alert_id);
    };

    /****************************************************************************
     *
     * Navigation
     *
     ****************************************************************************/

    $scope.goToOldAlert = function(alert_id){
        //$state.go('app.child_old_alert', { 'alert_id': alert_id, 'index': $scope.sa_id, 'pin': $scope.pin });
        $state.go('app.device_message_alert', { 'alert_id': alert_id, 'index': $scope.sa_id, 'pin': $scope.pin });
    };
    $scope.goToResources = function(index) {
        $state.go('app.resources', { 'index': index });
    };
    $scope.goToFollowers = function(index) {
        $state.go('app.followers', { 'index': index });
    };
    $scope.goToFollowing = function(index) {
        $state.go('app.following', { 'index': index });
    };
    $scope.goToPosts = function(index) {
        $state.go('app.posts', { 'index': index });
    };
});
