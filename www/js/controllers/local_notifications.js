var mod = angular.module('edify.controllers.localNotifications', []);

mod.controller("ExampleController", function($scope, $cordovaLocalNotification, $ionicPlatform) { //, $cordovaBadge) {

    $ionicPlatform.ready(function(){

        $scope.add = function () {
          var now = new Date().getTime();
          var _10SecondsFromNow = new Date(now + 10 * 1000);

          $cordovaLocalNotification.schedule({
            id: 1,
            title: 'This is a notification from Edify',
            text: "We've got some alerts you should see.",
            at: _10SecondsFromNow
          }).then(function (result) {
            // ...
          });
        };

        /*$scope.add = function() {
            var alarmTime = new Date();
            alarmTime.setMinutes(alarmTime.getMinutes() + 1);
            $cordovaLocalNotification.add({
                id: "1234",
                date: alarmTime,
                message: "Hi there,",
                title: "This is a notifcation from Edify.",
                autoCancel: true,
                sound: null
            }).then(function () {
                console.log("The notification has been set");
            });
        };*/

        $scope.isScheduled = function() {
            /*$cordovaLocalNotification.isScheduled("1234").then(function(isScheduled) {
                alert("Notification Scheduled: " + isScheduled);
            });*/
        };
        /*$ionicPlatform.ready(function() {
            $cordovaBadge.promptForPermission();

            $scope.setBadge = function(value) {
                $cordovaBadge.hasPermission().then(function(result) {
                    $cordovaBadge.set(value);
                }, function(error) {
                    alert(error);
                });
            }
        });*/
        /*
        $ionicPlatform.ready(function() {
            if(device.platform === "iOS") {
                window.plugin.notification.local.promptForPermission();
            }
        });
        */
    });
});