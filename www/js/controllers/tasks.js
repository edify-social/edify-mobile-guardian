var mod = angular.module('edify.controllers.tasks', []);

mod.controller('taskController', function(
    $scope,
    $ionicModal,
    $state,
    $http,
    $stateParams,
    $ionicLoading,
    ApiEndpoint,
    $rootScope
) {

    $scope.alertInit = function() {
        $scope.shouldShowDelete = false;
        $scope.listCanSwipe = true;
        $scope.linkedAccounts = JSON.parse(window.localStorage.getItem("local_account_list"));
        $scope.sa_id = $stateParams.index;
        $scope.pin = '';
        $scope.saved_alerts = [];
        var stored_notifications = '';

        angular.forEach($scope.linkedAccounts, function (v) {
            if ($scope.sa_id == v.edify_id) {
                $scope.pin = v.pin;
            }
        });

        // All alerts
        $scope.notification_list = window.localStorage.getItem("notifications_" + $scope.pin);
        if ($scope.notification_list === null) {
            $scope.notification_list = [];
            stored_notifications = [];
        } else if ($scope.notification_list.length == 0) {
            $scope.notification_list = [];
            stored_notifications = [];
        } else {
            stored_notifications = JSON.parse($scope.notification_list);
        }

        $scope.alerts = stored_notifications;
        // Saved Alerts
        angular.forEach($scope.alerts, function (v) {
            if (v.feedback == 'saved') {
                if($scope.saved_alerts){
                    $scope.saved_alerts.push(v);
                } else {
                    $scope.saved_alerts[0] = v;
                }
            }
        });

        if ($scope.saved_alerts) {
            if($scope.saved_alerts.length <= 0)
                $scope.hasNotifs = false;
            else
                $scope.hasNotifs = true;
        }
    };

    $scope.goToAlert = function(alert_id){
        $state.go('app.device_message_alert', { 'alert_id': alert_id, 'index': $scope.sa_id, 'pin': $scope.pin });        
    };

    /****************************************************************************
     *
     * Remove Notification
     *
     ****************************************************************************/

    $scope.remove_notif = function(alert_id){
        var notifications = JSON.parse($scope.notification_list);
        var object_place = 0;
        var current = {};
        angular.forEach(notifications, function(v, k){
            if(alert_id == v.id){
                current = v;
                object_place = k;
            }
        });
        notifications[object_place]['status'] = 'removed';
        notifications[object_place]['feedback'] = 'saved';
        notifications[object_place]['is_active'] = false;
        var headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        };
        var url = ApiEndpoint.url + "update_notifications";
        var postData =
            {
                "pin": $scope.pin,
                "notifications": notifications
            };
        return $http({
            method: 'POST',
            url: url,
            headers: headers,
            data: postData
        }).success(removeSuccessFn).error(removeErrorFn);
        function removeSuccessFn(data) {
            notifications.splice(object_place, 1);
            window.localStorage.setItem("notifications_" + $scope.pin, JSON.stringify(notifications));
            myNotifUpdt = JSON.parse(window.localStorage.getItem("notifications_" + $scope.pin));
            $rootScope.$broadcast('my_notifications:update', myNotifUpdt);
            $scope.alertInit();
            $ionicLoading.hide();
        }
        function removeErrorFn(data, error) {
            $ionicLoading.hide();
            console.error('Register error: ' + error + ', ' + JSON.stringify(data));
            $state.go('app.child_alert_list', { 'index': $scope.sa_id });
        }
    };

    $scope.removeNotification = function(alert_id){
        $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });
        $scope.remove_notif(alert_id);
    };

});
