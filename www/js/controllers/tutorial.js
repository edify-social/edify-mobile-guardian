var mod = angular.module('edify.controllers.tutorial', []);

// Empty on purpose - waiting for removal - used by badges
mod.controller('tutorialController', function($scope, $ionicSlideBoxDelegate, $state, $timeout) {
    // Called to navigate to the main app
    $scope.startApp = function() {
      $state.go('main');
    };
    $scope.next = function() {
      $ionicSlideBoxDelegate.next();
    };
    $scope.previous = function() {
      $ionicSlideBoxDelegate.previous();
    };

    // Called each time the slide changes
    $scope.slideChanged = function(index) {
      $scope.slideIndex = index;
    };

    $scope.goToDeviceT = function(alert_id){
        $state.go('app.device_tutorial');
    };
    $scope.goToFaqs = function(index) {
        $state.go('app.faqs_tutorial');
    };
    $scope.goToSocMedT = function(index) {
        $state.go('app.soc_med_tutorial');
    };
    $scope.goToOverviewT = function(index) {
        $state.go('app.overview_tutorial');
    };
});
