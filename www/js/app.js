var app = angular.module('edify', [
    'ionic',
    'ngCordova',
    'ngStorage',
    'ngResource',
    'angularMoment',
    'edify.controllers',
    'edify.services',
    'edify.utils'
]);

/****************************************************************************
 *
 * Constants
 *
 ****************************************************************************/

// Changing API Base URI one for authentication and the other for function calls
app.constant('ApiEndpoint', {
    //url: 'http://api.edify.dev:3000/v1/'
    url: 'https://api.edify.social/v1/'
});
app.constant('ImageEndpoint', {
    //url: 'http://api.edify.dev:3000'
    url: 'https://api.edify.social'
});

/****************************************************************************
 *
 * Directives
 *
 ****************************************************************************/

// Used to improve scrolling and slide menu behaviour 
app.directive('noScroll', function($document) {
  return {
    restrict: 'A',
    link: function($scope, $element, $attr) {

      $document.on('touchmove', function(e) {
        e.preventDefault();
      });
    }
  }
});

/****************************************************************************
 *
 * Main run with Ionic services
 *
 ****************************************************************************/

app.run(function($ionicPlatform, $rootScope) {

  $ionicPlatform.ready(function() {

    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }

    var notificationOpenedCallback = function(jsonData) {
      console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
    };

    window.plugins.OneSignal
      .startInit("8c010546-76ff-40f6-8761-5163eb01f8dd")
      .handleNotificationOpened(notificationOpenedCallback)
      .endInit();

      window.plugins.OneSignal.getPermissionSubscriptionState(function(status) {
        window.localStorage.setItem("player_id", JSON.stringify(status.subscriptionStatus.userId));
        window.localStorage.setItem("push_token", JSON.stringify(status.subscriptionStatus.pushToken));
      });

  });
});

/****************************************************************************
 *
 * App Router - URI Setup
 *
 ****************************************************************************/

app.config(function($stateProvider, $urlRouterProvider, $httpProvider, $ionicConfigProvider) {

    // From -io-config.json
    //{"app_id":"31813fae","api_key":"0ba05296813904463f3db756fbb490157abb2d046a8e7299","dev_push":true}

    $ionicConfigProvider.views.maxCache(0);

    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';

    $stateProvider

    // Menu (Main Route)
    .state('app', {
        url: "/app",
        abstract: true,
        templateUrl: "templates/menu.html",
        controller: 'AppCtrl'
    })

    /****************************************************************************
     *
     * Child Account  Routes
     *
     ****************************************************************************/

    // Attention Required List (Menu accessible)
    // index was indez
    .state('app.att_req', {
        url: "/attention/:index",
        params: {
            index: {
                value: '&1'
            }
        },
        views: {
            'menuContent': {
                templateUrl: "templates/child_account/att_req.html",
                controller: 'taskController'
            }
        }
    })
    // On boarding guide (Menu accessible)
    .state('app.onboarding', {
        url: "/onboarding/:index",
        cache: false,
        params: {
            index: {
                value: '&1'
            }
        },
        views: {
            'menuContent': {
                templateUrl: "templates/child_account/onboarding_guide.html",
                controller: "homeController"
            }
        }
    })
    .state('app.sub_acct_config', {
        url: "/subprofileconfig/:index",
        cache: false,
        params: {
            index: {
                value: '&1'
            }
        },
        views: {
            'menuContent': {
                templateUrl: "templates/child_account/sub_acct_config.html",
                controller: 'linkController'
            }
        }
    })
    .state('app.device_messages', {
        url: "/device_messages/:index",
        params: {
            index: {
                value: '&1'
            }
        },
        views: {
            'menuContent': {
                templateUrl: "templates/child_account/device_messages.html",
                controller: 'DeviceMessagesController'
            }
        }
    })
    .state('app.backtracked', {
        url: "/backtracked/:index",
        params: {
            index: {
                value: '&1'
            }
        },
        views: {
            'menuContent': {
                templateUrl: "templates/child_account/dms_backtrack.html",
                controller: 'BacktrackedMessagesController'
            }
        }
    })
    .state('app.dm_backtrack_alert', {
        url: "/dm_backtrack_alert/:index",
        params: {
            alert_id: {
                value: '&1'
            },
            index: {
                value: '&2'
            },
            pin: {
                value: '&3'
            }
        },
        views: {
            'menuContent': {
                templateUrl: "templates/child_account/dm_backtrack_alert.html",
                controller: 'BacktrackedMessageAlertController'
            }
        }
    })
    .state('app.device_message_alert', {
        url: "/device_messages_alert/:index",
        params: {
          alert_id: {
              value: '&1'
          },
          index: {
              value: '&2'
          },
          pin: {
              value: '&3'
          }
        },
        views: {
            'menuContent': {
                templateUrl: "templates/child_account/device_message_alert.html",
                controller: 'DeviceChildAlertController'
            }
        }
    })
    .state('app.app_list', {
        url: "/app_list/:index",
        params: {
            index: {
                value: '&1'
            }
        },
        views: {
            'menuContent': {
                templateUrl: "templates/child_account/app_list.html",
                controller: 'AppListController'
            }
        }
    })
    .state('app.instagram_config', {
        url: "/instagram_config/:child_id",
        cache: false,
        params: {
            child_id: {
                value: '&1'
            }
        },
        views: {
            'menuContent': {
                templateUrl: "templates/child_account/instagram_config.html",
                controller: 'SocProfileController'
            }
        }
    })

    /****************************************************************************
     *
     * Session Routes
     *
     ****************************************************************************/

    // Login
    .state('login', {
        url: '/login',
        templateUrl: 'templates/session/login.html'
    })
    // Register
    .state('register', {
        url: '/register',
        templateUrl: 'templates/session/signup.html'
    })
    // When email activation has been sent
    .state('activation_sent', {
        url: '/activation_sent',
        templateUrl: 'templates/session/activation_sent.html'
    })
    // When email activation is pending
    .state('activation_pending', {
        url: '/activation_pending',
        templateUrl: 'templates/session/activation_pending.html'
    })

    /****************************************************************************
     *
     * Guardian Routes
     *
     ****************************************************************************/

    // User's Home Screen (Menu accessible)
    .state('app.overview', {
        url: "/overview",
        cache: false,
        views: {
            'menuContent': {
                templateUrl: "templates/guardian/overview.html",
                controller: 'homeController'
            }
        }
    })
    // Resources (Menu accessible)
    .state('app.resources', {
        url: '/resources',
        views: {
            'menuContent': {
                templateUrl: 'templates/guardian/resources.html',
                controller: 'resourceController'
            }
        }
    })
    // Tutorial (Menu accessible)
    .state('app.tutorial', {
        url: '/tutorial',
        views: {
            'menuContent': {
                templateUrl: 'templates/guardian/tutorial.html',
                controller: 'tutorialController'
            }
        }
    })
    // User Profile
    .state('app.profile', {
        url: "/profile",
        views: {
            'menuContent': {
                templateUrl: "templates/guardian/profile.html",
                controller: 'authenticationController'
            }
        }
    })

    /****************************************************************************
     *
     * Tutorial Routes
     *
     ****************************************************************************/
     // Overview Tutorial
     .state('app.overview_tutorial', {
         url: '/overview_tutorial',
         views: {
             'menuContent': {
                 templateUrl: 'templates/tutorials/overview_tutorial.html',
                 controller: 'tutorialController'
             }
         }
     })
     // Device Association Tutorial
     .state('app.device_tutorial', {
         url: '/device_tutorial',
         views: {
             'menuContent': {
                 templateUrl: 'templates/tutorials/device_tutorial.html',
                 controller: 'tutorialController'
             }
         }
     })
     // Social Media Notificaions Tutorial
     .state('app.soc_med_tutorial', {
         url: '/soc_med_tutorial',
         views: {
             'menuContent': {
                 templateUrl: 'templates/tutorials/soc_med_tutorial.html',
                 controller: 'tutorialController'
             }
         }
     })
     // FAQs
     .state('app.faqs_tutorial', {
         url: '/faqs_tutorial',
         views: {
             'menuContent': {
                 templateUrl: 'templates/tutorials/faqs_tutorial.html',
                 controller: 'tutorialController'
             }
         }
     })
    /****************************************************************************
     *
     * Social Media Routes
     *
     ****************************************************************************/

    // Child Alert
    .state('app.child_alert', {
        url: "/child_alert/:alert_id",
        cache: false,
        params: {
            alert_id: {
                value: '&1'
            },
            index: {
                value: '&2'
            },
            pin: {
                value: '&3'
            }
        },
        views: {
            'menuContent': {
                templateUrl: "templates/social_media/instagram/child_alert.html",
                controller: 'ChildAlertController'
            }
        }
    })
    .state('app.child_old_alert', {
        url: "/child_old_alert/:alert_id",
        params: {
            alert_id: {
                value: '&1'
            },
            index: {
                value: '&2'
            },
            pin: {
                value: '&3'
            }
        },
        views: {
            'menuContent': {
                templateUrl: "templates/social_media/instagram/child_alert_old.html",
                controller: 'DeviceChildAlertController'
            }
        }
    })
    .state('app.lk_user', {
        url: '/lk_user/:lkToken/:uid',
        params: {
            //lkToken: { value: '&1' },
            uid: { value: '&1' }
        },
        views: {
            'menuContent': {
                templateUrl: 'templates/social_media/instagram/lk_user.html',
                controller: 'SocProfileController'
            }
        }
    })
    .state('app.followers', {
        url: '/followers',
        views: {
            'menuContent': {
                templateUrl: 'templates/social_media/instagram/followers_list.html',
                controller: 'SocProfileController'
            }
        }
    })
    .state('app.following', {
        url: '/following',
        views: {
            'menuContent': {
                templateUrl: 'templates/social_media/instagram/following_list.html',
                controller: 'SocProfileController'
            }
        }
    })
    .state('app.posts', {
        url: '/posts',
        views: {
            'menuContent': {
                templateUrl: 'templates/social_media/instagram/posts.html',
                controller: 'SocProfileController'
            }
        }
    })
    // Sub account alert list
    .state('app.child_alert_list', {
        url: '/child_alert_list/:index',
        params: {
            index: {
                value: '&1'
            }
        },
        views: {
            'menuContent': {
                templateUrl: 'templates/social_media/instagram/child_alert_list.html',
                controller: 'ChildAlertsController'
            }
        }
    });

    $urlRouterProvider.otherwise('/login');

});
