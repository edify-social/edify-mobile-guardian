var mod = angular.module('edify.controllers', [
  'edify.controllers.app_main',
  'edify.controllers.authentication',
  'edify.controllers.home',
  'edify.controllers.linkAccount',
  'edify.controllers.localNotifications',
  'edify.controllers.socialPlatform',
  'edify.controllers.resources',
  'edify.controllers.tutorial',
  'edify.controllers.tasks',
  'edify.controllers.child_alerts',
  'edify.controllers.child_alert',
  'edify.controllers.app_list',
  'edify.controllers.device_messages',
  'edify.controllers.backtracked_messages',
  'edify.controllers.backtrack_msg',
  'edify.controllers.device_child_alert'
]);
