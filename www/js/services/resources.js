var mod = angular.module('edify.services.resources', []);


mod.service('ResourceService', function (
    $http,
    $timeout,
    ApiEndpoint
) {

    /****************************************************************************
     *
     * Resource Poll
     * - Console variables:
     *  - resources
     *
     ****************************************************************************/

    var self = {

        getData: function () {
            var loadTime = 5000,     //Load the data every second
                errorCount = 0,        //Counter for the server errors
                loadPromise;                //Pointer to the promise created by the Angular $timout service
            var resource_list, url;
            url = ApiEndpoint.url + "resources";
            $http({
                url: url,
                method: "GET"
            })
            .then(function (res) {

                if (res.status == 200) {
                    window.localStorage.setItem("edify_resource_list", JSON.stringify(res.data.resources));
                    resource_list = JSON.parse(window.localStorage.getItem("edify_resource_list"));
                }

                errorCount = 0;
                nextLoad();
            })
            .catch(function (res) {
                if (res.data) {
                    console.log('Server error: ' + JSON.stringify(res.data.info));
                } else {
                    console.log('Server error: ' + JSON.stringify(res.data));
                }
                nextLoad(++errorCount * 2 * loadTime);
            });

            function cancelNextLoad() {
                $timeout.cancel(loadPromise);
            }

            function nextLoad(mill) {
                mill = mill || loadTime;
                cancelNextLoad();
                $timeout(self.getData, mill);
            }
        }
    };

    return self;
});
