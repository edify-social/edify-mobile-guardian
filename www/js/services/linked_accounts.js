var mod = angular.module('edify.services.LinkedAccountsServices', []);


/****************************************************************************
 *
 * User's Linked Accounts
 *
 ****************************************************************************/

mod.service('LinkedAccountService', function (
    $q,
    $rootScope,
    $localstorage,
    $ionicPopup,
    $ionicHistory,
    $ionicLoading,
    ApiEndpoint,
    ImageEndpoint,
    NotificationPollService,
    $http,
    $state
){
    var self = {

        /***************************************************************
         *
         * Sub account ID array handlers
         *
         ***************************************************************/

        getIdArray: function () {
            var id_arr = '';
            // Current local accounts by id - easier to check then the accounts objects
            if (window.localStorage.getItem("local_link_acct_ids")) {
                id_arr = JSON.parse(window.localStorage.getItem("local_link_acct_ids"));
            }
            else {
                window.localStorage.setItem("local_link_acct_ids", []);
                id_arr = [];
            }
            return id_arr;
        },

        incrementIdArray: function () {
            var ids, last_number, next_id;
            ids = self.getIdArray().sort();
            if(ids.length == 0){
                last_number = 0;
            } else {
                last_number = Number(ids[ids.length - 1]);
            }
            next_id = last_number + 1;
            ids.push(next_id);
            window.localStorage.setItem("local_link_acct_ids", JSON.stringify(ids));
            return next_id;
        },

        removeIdArray: function (subid) {
            var id_arr;
            id_arr = JSON.parse(window.localStorage.getItem("local_link_acct_ids"));
            foundAt = id_arr.indexOf(subid);
            id_arr.splice(foundAt, 1);
            window.localStorage.setItem("local_link_acct_ids", JSON.stringify(id_arr));
        },

        /***************************************************************
         *
         * Sub account handlers
         *
         ***************************************************************/

        genEdifyIdList: function(remoteList) {
            var all_accts;
            var edid = [];
            if(remoteList == 0){
                pre_all_accts = window.localStorage.getItem("local_account_list");
                if(pre_all_accts){
                    all_accts = JSON.parse(pre_all_accts);
                } else {
                    all_accts = [];
                }
            } else {
                all_accts = remoteList;
            }
            if(!all_accts){
                // If all accts is null then define as empty
                all_accts = [];
            }
            if(all_accts.length > 0) {
                angular.forEach(all_accts, function (val) {
                    if(remoteList == 0) {
                        edid.push(val["edify_id"])
                    } else {
                        edid.push(val["child_id"])
                    }
                });
                window.edf = edid;
                return edid;
            } else {
                window.edfl = edid;
                return all_accts;
            }
        },

        checkAccountRemovals: function(local_accts, remoteList, runthis) {
            // Ask Local
            // Remove if not exists
            var child_id;

            edidRem = self.genEdifyIdList(remoteList);

            if(runthis) {
                angular.forEach(local_accts, function (val) {
                    child_id = val["child_id"];
                    if (edidRem.indexOf(child_id) > 0) {
                        self.removeAcct(val.id, 1);
                    }
                });
            }
        },

        updateAccountList: function(uid, remoteList) {
            // Remote is our single source of truth

            // TODO: Change this to promise
            // TODO: Add sync for offline creation of accounts

            var sname, child_id, child_phone, savatar, insta_email, child_pin,
                backtrack;
            var runme = true;

            pre_localAccts = window.localStorage.getItem("local_account_list");
            if(pre_localAccts){
                local_accts = JSON.parse(pre_localAccts);
            } else {
                local_accts = [];
            }

            // Check in local accounts
            edidLoc = self.genEdifyIdList(0);

            // Ask Remote
            // Create if not exists
            angular.forEach(remoteList, function(v){
                child_id            = v["child_id"];
                sname               = v["child_name"];
                child_phone         = v["phone"];
                savatar             = ImageEndpoint.url + v["image"]["image"]["url"];
                insta_email         = v["instagram_user"];
                child_pin           = v["pin"];
                backtrack_status    = v["backtrack_status"];
                if(edidLoc.indexOf(child_id) < 0){
                    self.createAccount(uid, sname, child_phone, savatar, insta_email, 1, child_pin, child_id, backtrack_status);
                }
            });

            $ionicLoading.hide();
            self.checkAccountRemovals(local_accts, remoteList, runme);
        },

        removeAcct: function(val, local){
            var subid, all_accts, current, object_place, edify_id, pin;

            subid = Number(val);
            all_accts = JSON.parse(window.localStorage.getItem("local_account_list"));

            // Remove from ID Array
            self.removeIdArray(subid);

            // Remove Accounts
            angular.forEach(all_accts, function(v, k){
                if(subid == v.id){
                    current = v;
                    object_place = k;
                    pin = v.pin;
                    edify_id = v.edify_id;
                }
            });

            window.localStorage.removeItem("notifications_" + pin);
            window.localStorage.removeItem("notifications_backtrack_" + pin);
            all_accts.splice(object_place, 1);
            window.localStorage.setItem("local_account_list", JSON.stringify(all_accts));

            $ionicHistory.nextViewOptions({
                disableBack: true
            });

            data = {
                "edify_id": edify_id
            };
            if(local == 0) {
                var headers = {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                };
                var url = ApiEndpoint.url + 'child_onboarding/remove_child';
                return $http({
                    method: 'DELETE',
                    url: url,
                    headers: headers,
                    data: data
                }).success(removeSuccessFn).error(removeErrorFn);
            }

            // Broadcast account removal
            acctUpdt = JSON.parse(window.localStorage.getItem("local_account_list"));
            $rootScope.$broadcast('linked_accts:update', acctUpdt);

            function removeSuccessFn(data) {
                $ionicLoading.hide();
                $state.go('app.overview');
            }

            function removeErrorFn(data, error) {
                $ionicLoading.hide();
                console.error('Register error: ' + error + ', ' + JSON.stringify(data));
            }
        },

        createAccount: function (uid, sname, child_phone, savatar, insta_email, local, chpin, chid, backtrack_status) {
            // Setup the loader
            $ionicLoading.show({
                content: 'Loading',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showDelay: 0
            });
            var anAccount = {};
            var temp_linked_accts;
            var next_id = self.incrementIdArray();
            var headers = {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            };
            var url = ApiEndpoint.url + 'child_onboarding/add_child';
            // List of accounts
            if (window.localStorage.getItem("local_account_list")) {
                temp_linked_accts = JSON.parse(window.localStorage.getItem("local_account_list"));
            }
            else {
                window.localStorage.setItem("local_account_list", []);
                temp_linked_accts = [];
            }

            // If local == 0 it means that it will create an account on the server
            if(local == 0) {
                anAccount =
                {
                    "id": next_id,
                    "name": sname,
                    "edify_user": uid,
                    "child_phone": child_phone,
                    "edify_id": '',
                    "pin": '',
                    "instagram_user": insta_email
                };
                return $http({
                    method: 'POST',
                    url: url,
                    headers: headers,
                    data: anAccount
                }).success(registerSuccessFn).error(registerErrorFn);
            } else {
                anAccount =
                {
                    "id": next_id,
                    "name": sname,
                    "edify_user": uid,
                    "child_phone": child_phone,
                    "edify_id": chid,
                    "image": savatar,
                    "pin": chpin,
                    "backtrack_status": backtrack_status,
                    "instagram_user": insta_email
                };
                temp_linked_accts.push(anAccount);

                // TODO: Needs to check if its not repeated. Based on PIN?
                window.localStorage.setItem("local_account_list", JSON.stringify(temp_linked_accts));
                acctUpdt = JSON.parse(window.localStorage.getItem("local_account_list"));
                $rootScope.$broadcast('linked_accts:update', acctUpdt);

                //NotificationPollService.checkData();
                $ionicLoading.hide();
                return window.localStorage.getItem("local_account_list");

            }

            function registerSuccessFn(data) {
                anAccount.edify_id = data.data.id;
                anAccount.pin = data.data.pin;
                if(data.data.image) {
                    anAccount.image =  ImageEndpoint.url + data.data.image;
                }

                temp_linked_accts.push(anAccount);

                // TODO: Needs to check if its not repeated. Based on PIN?
                window.localStorage.setItem("local_account_list", JSON.stringify(temp_linked_accts));
                acctUpdt = JSON.parse(window.localStorage.getItem("local_account_list"));
                $rootScope.$broadcast('linked_accts:update', acctUpdt);

                $ionicLoading.hide();
                // TODO: Store data from server such as repeated account error
                window.crdata = data;
                $state.go('app.sub_acct_config', { 'index': next_id });
                //$state.go('app.onboarding', { 'index': next_id });
            }

            function registerErrorFn(data, error) {
                // TODO: Why would this store the account even when it has failed?
                // temp_linked_accts.push(anAccount);
                // window.localStorage.setItem("local_account_list", JSON.stringify(temp_linked_accts));

                $ionicLoading.hide();
                console.error('Register error: ' + error + ', ' + JSON.stringify(data));
            }

        },

        updateChildAccount: function (acctId, newName, newPhone) {
            var edid, all_accts, current, object_place;

            edid = Number(acctId);
            all_accts = window.als =  JSON.parse(window.localStorage.getItem("local_account_list"));

            // Server
            var headers = {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            };
            var newProfile ={
                "id": acctId,
                "child_name": newName,
                "child_phone": newPhone
            };
            var url = ApiEndpoint.url + 'update_child';
            $http({
                method: 'POST',
                url: url,
                headers: headers,
                data: newProfile
            }).success(registerSuccessFn).error(registerErrorFn);

            function registerSuccessFn(data) {
                angular.forEach(all_accts, function(v, k){
                    if(edid == v.edify_id){
                        current = v;
                        object_place = k;
                    }
                });

                all_accts[object_place].name = newName;
                all_accts[object_place].child_phone = newPhone;
                window.localStorage.setItem("local_account_list", JSON.stringify(all_accts));
                acctUpdt = JSON.parse(window.localStorage.getItem("local_account_list"));
                $rootScope.$broadcast('linked_accts:update', acctUpdt);

                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: "<h2 style='color: #2d7abe !important;'>Success</h2>",
                    subTitle: 'Changes saved on server',
                    template: "Account updated."
                });

                return all_accts;
            }

            function registerErrorFn(data, error) {
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: "<h2>Error</h2>",
                    subTitle: 'No changes saved on server',
                    template: "Account could not be updated."
                });
            }
        },

        updateAccountAvatar: function (acctId, newAvatar) {
            var subid, all_accts, current, object_place, url_img, updAccts;

            subid = Number(acctId);
            all_accts = JSON.parse(window.localStorage.getItem("local_account_list"));

            var headers = {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            };
            var avatarData ={
                "id": acctId,
                "base64_avatar": newAvatar
            };
            var url = ApiEndpoint.url + 'upload';
            return $http({
                method: 'POST',
                url: url,
                headers: headers,
                data: avatarData
            }).success(avatarSuccessFn).error(avatarErrorFn);

            function avatarSuccessFn(data) {
              // Remove Accounts
              angular.forEach(all_accts, function(v, k){
                  if(subid == v.edify_id){
                      current = v;
                      object_place = k;
                  }
              });

              url_img = ImageEndpoint.url + data.data.image_url.image.url;
              all_accts[object_place].image = url_img;
              window.localStorage.setItem("local_account_list", JSON.stringify(all_accts));
              updAccts = JSON.parse(window.localStorage.getItem("local_account_list"));

              $rootScope.$broadcast('linked_accts:update', updAccts);

              return url_img;
            }

            function avatarErrorFn(data, error) {
              $ionicLoading.hide();
              $ionicPopup.alert({
                  title: "<h2>Error</h2>",
                  subTitle: 'No changes saved on server',
                  template: "Avatar could not be updated."
              });
            }
        },

        updateAccountInstagram: function (acctId, newUsr, newPaswd) {
            var subid, all_accts, current, object_place;
            subid = Number(acctId);
            all_accts =  JSON.parse(window.localStorage.getItem("local_account_list"));
            var headers = {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            };
            var insta_creds ={
                "id": acctId,
                "instagram_username": newUsr,
                "instagram_password": newPaswd
            };
            var url = ApiEndpoint.url + 'update_instagram';
            return $http({
                method: 'POST',
                url: url,
                headers: headers,
                data: insta_creds
            }).success(registerSuccessFn).error(registerErrorFn);

            function registerSuccessFn(data) {

                angular.forEach(all_accts, function(v, k){
                    if(subid == v.edify_id){
                        current = v;
                        object_place = k;
                    }
                });

                all_accts[object_place].instagram_user = newUsr;
                window.localStorage.setItem("local_account_list", JSON.stringify(all_accts));

                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: "<h2 style='color: #2d7abe !important;'>Success</h2>",
                    subTitle: 'Changes saved on server',
                    template: "Instagram credentials updated."
                });

                return all_accts;
            }

            function registerErrorFn(data, error) {
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: "<h2>Error</h2>",
                    subTitle: 'No changes saved on server',
                    template: "Instagram credentials could not be updated."
                });
            }
        },

        resetBacktrack: function (acctId, pin) {
            var current, object_place;
            var all_accts = JSON.parse(window.localStorage.getItem("local_account_list"));
            subid = Number(acctId);
            var headers = {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            };
            var url = ApiEndpoint.url + "request_backtrack";
            var postData =
                {
                    "pin": pin
                };
            return $http({
                method: 'POST',
                url: url,
                headers: headers,
                data: postData
            }).success(resetSuccessFn).error(resetErrorFn);

            function resetSuccessFn(data) {
                console.log("Request sent to server");
                console.log(data);

                // Get current account set to object and account position through params id
                angular.forEach(all_accts, function(v, k){
                    console.log(subid);
                    console.log(v.id);
                    if(subid  == v.edify_id){
                        current = v;
                        object_place = k;
                    }
                });

                all_accts[object_place].backtrack_status = 0;
                window.localStorage.setItem("local_account_list", JSON.stringify(all_accts));

                $ionicPopup.show({
                    title: "<h2>Success</h2>",
                    subTitle: "Request sent",
                    template: 'Your backtrack request has been queued.',
                    buttons: [
                        {
                            text: 'OK',
                            type: 'button-positive'
                        }
                    ]
                });
            }
            function resetErrorFn(data, error) {
                console.error('Request error: ' + error + ', ' + JSON.stringify(data));
            }
        }
    };

	return self;

});
