var mod = angular.module('edify.services.userServices', []);


mod.service('UserService', function (
	$q,
	$rootScope,
	$localstorage,
	$ionicPopup,
	$ionicLoading,
	$state,
	ApiEndpoint,
	LinkedAccountService,
	$http
) {

	var self = {

		/****************************************************************************
		*
		* Update
		*
		****************************************************************************/

		updateUser: function (upd){
			// Load user
			$ionicLoading.show({
				content: 'Loading',
				animation: 'fade-in',
				showBackdrop: true,
				maxWidth: 200,
				showDelay: 0
			});

			var token = window.localStorage.getItem("edify_user_token");
			var headers = {
				'Content-Type': 'application/json',
				'Accept': 'application/json',
				'Authorization': 'Token ' + token
			  };

			var url = ApiEndpoint.url + 'update_user';

			return $http({
			  method: 'POST',
			  url: url,
			  headers: headers,
			  data: upd
			})
			.success(updateSuccessFn).error(updateErrorFn);

			function updateSuccessFn(data) {
				// Update stored user instance
				window.localStorage.setItem("edify_user_email", data.data.email);
				window.localStorage.setItem("edify_user_phone", data.data.phone);
				window.localStorage.setItem("edify_user_name", data.data.name);

				console.log(data);
				$ionicPopup.alert({
					title: "<h2>Updated</h2>",
                    subTitle: 'Update Successful',
					template: "Your information has been updated successfully."
				});

				$ionicLoading.hide();
				//$state.go('app.overview');
			}

			function updateErrorFn(data, error, status, headers, config) {
				$ionicLoading.hide();
				$ionicPopup.alert({
					title: "<h2>Error</h2>",
					subTitle: 'Your profile was not updated',
					template: "Server replied with the following error: " + data.info[0]
				});
				console.error('Update error: ' + error + ', ' + JSON.stringify(data));
			}
		},

		/****************************************************************************
		*
		* Registration
		*
		****************************************************************************/

		createUser: function(reg) {
			var headers = {
				'Content-Type': 'application/json',
				'Accept': 'application/json'
			};

			var url = ApiEndpoint.url + 'registrations/';

			return $http({
			  method: 'POST',
			  url: url,
			  headers: headers,
			  data: reg
			})
			.success(registerSuccessFn).error(registerErrorFn);

			function registerSuccessFn(data) {
				window.localStorage.setItem("edify_user_token", data.data.auth_token);
				window.localStorage.setItem("edify_user_id", data.data.id);
				window.localStorage.setItem("edify_user_name", data.data.name);
				window.localStorage.setItem("edify_user_phone", data.data.phone);
				window.localStorage.setItem("edify_user_email", data.data.email);

			  //$state.go('activation_sent');
			}

			function registerErrorFn(data, error, status, headers, config) {
			  console.error('Register error: ' + error + ', ' + JSON.stringify(data));
			}
		},

		/****************************************************************************
		 *
		 * Load User
		 *
		 ****************************************************************************/

		loadUser: function (edify_id) {
			var url = ApiEndpoint.url + 'user/' + edify_id;
			var headers = {
				'Content-Type': 'application/json',
				'Accept': 'application/json',
				'Authorization': 'Token ' + window.localStorage.getItem("edify_user_token")
			};
			return $http({
			  method: 'GET',
			  url: url,
			  headers: headers
			})
			.success(meSuccessFn).error(meErrorFn);

			// Success and Error functions
			function meSuccessFn(data) {
				window.localStorage.setItem("edify_user_id", data.user.id);
				window.localStorage.setItem("edify_user_name", data.user.name);
				window.localStorage.setItem("edify_user_phone", data.user.phone);
				window.localStorage.setItem("edify_user_email", data.user.email);
                window.localStorage.setItem("backtrack_purchased", data.user.backtrack);
        LinkedAccountService.updateAccountList(edify_id, data.user.edify_children);
			}
			function meErrorFn(data, error, status, headers, config) {
				console.error('Info requesting error: ' + error + ', ' + JSON.stringify(data));
			}
		},

        /****************************************************************************
         *
         * Load Child Accounts
         *
         ****************************************************************************/

        loadChildAccounts: function (edify_id) {
            var url = ApiEndpoint.url + 'user/' + edify_id;
            var headers = {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Token ' + window.localStorage.getItem("edify_user_token")
            };
            return $http({
                method: 'GET',
                url: url,
                headers: headers
            }).success(meSuccessFn).error(meErrorFn);

            // Success and Error functions
            function meSuccessFn(data) {
                new_act_list = LinkedAccountService.updateAccountList(edify_id, data.user.edify_children);
                return new_act_list;
            }
            function meErrorFn(data, error) {
                $ionicLoading.hide();
                console.error('Info requesting error: ' + error + ', ' + JSON.stringify(data));
            }
        },

		/****************************************************************************
		 *
		 * Logout
		 *
		 ****************************************************************************/

		logoutUser: function () {
			var d = $q.defer();
			var headers = {
				'Authorization':   'Token ' + window.localStorage.getItem("edify_user_token"),
				'Content-Type': 'application/json',
				'Accept': 'application/json'
			};

			var url = ApiEndpoint.url + 'sessions/';
			return $http({
			  method: 'DELETE',
			  url: url,
			  headers: headers
			})
			.success(logoutSuccessFn)
			.error(logoutErrorFn);

			function logoutSuccessFn(data, status, headers, config) {
                localStorage.clear();

				$ionicLoading.hide();
				$state.go('login');
			}

			function logoutErrorFn(data, status, headers, config) {
			  console.error('Logout Error: ' + JSON.stringify(data));
				$ionicLoading.hide();
				d.reject('Logout Error.');
			}
		},

		/****************************************************************************
		 *
		 * Login
		 *
		 ****************************************************************************/

		loginUser: function (lfd) {
			var d = $q.defer();
			var url = ApiEndpoint.url + 'sessions/';
			var headers = {
				'Content-Type': 'application/json',
				'Accept': 'application/json'
			};
			$http({
			  method: 'POST',
			  url: url,
			  headers: headers,
			  data: lfd
			})
			.success(loginSuccessFn).error(loginErrorFn);

			function loginSuccessFn(data) {
				window.localStorage.setItem("edify_user_token", data.authentication_token);
                window.localStorage.setItem("edify_user_id", data.id);
				d.resolve(data.authentication_token);
			}

			function loginErrorFn(data, error) {
			  if(data.non_field_errors){
				if(data.non_field_errors == "User account is disabled."){
					//d.reject('actPending');
					$state.go('activation_pending');
				} else {
					d.reject(data.non_field_errors);
					$ionicPopup.alert({
						title: "<h2>Error</h2>",
						template: data.non_field_errors
					});
				}
			  } else {
				  window.errr = data;
				  $ionicPopup.alert({
					  title: "<h2>Error</h2>",
					  template: 'Something went wrong: ' + JSON.stringify(data.error)
				  });
				  $ionicLoading.hide();

			  }

			}
			return d.promise;
		}
	};

	return self;
})
;
