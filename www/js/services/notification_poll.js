/****************************************************************************
 * Server Poll
 * Service polles the server for alerts and created local notifications.
 * Substituted by push notifications. Read
 * https://gitlab.randomprimate.com/Edify/edify-mobile-guardian/snippets/11
 * for reference about home.js requirements.
 ****************************************************************************/


var mod = angular.module('edify.services.notification_poll', []);


mod.service('NotificationPollService', function (
    $http,
    $timeout,
    $rootScope,
    $ionicPlatform,
    ApiEndpoint
    //$cordovaLocalNotification
) {

    /****************************************************************************
     *
     * Server Poll
     * - Console variables:
     *  - children
     *  - notifs
     *
     ****************************************************************************/

     // Generate local notifcations
     //$ionicPlatform.ready(function () {

       // Local notification has been replaced by push notifications
       /*if(device.platform === "iOS") {
            window.plugin.notification.local.promptForPermission();
        }*/

        /*$rootScope.scheduleInstantNotification = function () {
             $cordovaLocalNotification.schedule({
             id: 1,
             text: 'You have one or more pending alert.',
             title: "Edify Alert",
             sound: "file://sounds/reminder.mp3"
           }).then(function () {
             // Alert sent
           });
         };*/

     //});

    self = {

        /****************************************************************************
         *
         * Create notification object
         * For each notification encountered create
         * a custom notification list and populate it
         *
         ****************************************************************************/


        update_notifications: function(notif, pin) {
            var stored_ch_notif = '';
            var notif_child = window.localStorage.getItem("notifications_" + pin);
            if(notif_child === null){
                window.localStorage.setItem("notifications_" + pin, []);
                stored_ch_notif = [];
            }
            else if(notif_child.length == 0) {
                stored_ch_notif = [];
            } else {
                stored_ch_notif = JSON.parse(notif_child);
            }
            var obj = stored_ch_notif.filter(function ( obj )
            {
                return obj.id == notif.id
            });
            if(obj){
                if(obj.length > 0){
                    // Found a notification
                    // Action: Skip
                } else {
                    // Empty: no notification found. New notification.
                    stored_ch_notif.push(notif);
                    window.localStorage.setItem("notifications_" + pin, JSON.stringify(stored_ch_notif));
                    myNotifUpdt = JSON.parse(window.localStorage.getItem("notifications_" + pin));
                    $rootScope.$broadcast('my_notifications:update', myNotifUpdt);
                    //$rootScope.scheduleInstantNotification();
                }
            }
        },

        /****************************************************************************
         *
         * Create backtracked notification object
         * For each notification encountered create
         * a custom notification list and populate it
         * where backtrack is true
         *
         ****************************************************************************/


        update_backtracked_notifications: function(notif, pin) {
            var stored_ch_notif = '';
            var notif_child = window.localStorage.getItem("notifications_backtrack_" + pin);
            if(notif_child === null){
                window.localStorage.setItem("notifications_backtrack_" + pin, []);
                stored_ch_notif = [];
            }
            else if(notif_child.length == 0) {
                stored_ch_notif = [];
            } else {
                stored_ch_notif = JSON.parse(notif_child);
            }
            var obj = stored_ch_notif.filter(function ( obj )
            {
                return obj.id == notif.id
            });
            if(obj){
                if(obj.length > 0){
                    // Found a notification
                    // Action: Skip
                } else {
                    // Empty: no notification found. New notification.
                    stored_ch_notif.push(notif);
                    window.localStorage.setItem("notifications_backtrack_" + pin, JSON.stringify(stored_ch_notif));
                    myNotifUpdt = JSON.parse(window.localStorage.getItem("notifications_backtrack_" + pin));
                    $rootScope.$broadcast('my_notifications:update', myNotifUpdt);
                    //$rootScope.scheduleInstantNotification();
                }
            }
        },

        /****************************************************************************
         *
         * Poll server
         * For each account request notifications
         *
         ****************************************************************************/

        getData: function(pin) {
            var notification = {};
            pre_children = window.localStorage.getItem("local_account_list");
            if(pre_children){
                children = JSON.parse(pre_children);
            } else {
                children = [];
            }
            var url = ApiEndpoint.url + "notification_poll/";
            var postData =
                {
                    "pin": pin
                };

            $http({
                url: url,
                method: "POST",
                data: postData
            })
            .then(function(res) {
                if(res.status == 200 && res.data.info != 'No notifications yet'){
                    window.res = res;
                    angular.forEach(res.data.notifications, function(v, k){
                        notification.id             = v['id'];
                        notification.user_phone     = v['child_phone'];
                        notification.text           = v['content'];
                        notification.contact_number = v['contact_number'];
                        notification.user           = v['user'];
                        notification.label          = v['label'];
                        notification.notif_type     = v['notification_type'];
                        notification.feedback       = v['feedback'];
                        notification.is_active      = v['is_active'];
                        notification.created_at     = v['created_at'];
                        notification.context        = v['context'];
                        notification.msg_id         = v['instagram_message_id'];
                        notification.pin            = pin;
                        notification.sent           = v['sent'];
                        backtracked                 = v['backtrack'];
                        if(backtracked == 'true' || backtracked == true){
                            self.update_backtracked_notifications(notification, pin);
                        } else {
                            self.update_notifications(notification, pin);
                        }
                    });
                } else {
                    console.log('No notifications available.');
                }

            })
            .catch(function(res) {
                if(res.data){
                    console.log('Server error: ' + JSON.stringify(res.data.info));
                } else {
                    console.log('Server error: ' + JSON.stringify(res.data));
                }
            });
        },

        /****************************************************************************
         *
         * Check if polling is required
         * based on existence of child accounts
         *
         ****************************************************************************/

        checkData: function()
        {
            console.log("notifs");
            var pre_children = window.localStorage.getItem("local_account_list");
            var children = [];
            if(pre_children)
                children = JSON.parse(pre_children);
            if (children) {
                if (children.length > 0) {
                    angular.forEach(children, function(v, k){
                        self.getData(v.pin);
                    });
                } else {
                    console.log("no accounts found");
                }
            }
        }
    };

    return self;

});
